@extends('layout.index')

@section('content')

<!-- Page Content -->
<div class="container">
    <div class="row">

        @include('layout.menu')

        <div class="col-md-9 ">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#337AB7; color:white;">
                    <h4> <b>{{$types->Name}} </b></h4>
                </div>
                @foreach($newses as $news)
                    <div class="row-item row">
                        <div class="col-md-3">
                            <a href="news/{{$news->id}}/{{$news->unsignedTitle}}.html">
                                <br>
                                <img width="200px" height="200px" class="img-responsive" src="upload/news/{{$news->Image}}" alt="">
                            </a>
                        </div>

                        <div class="col-md-9">
                            <h3> <a href="news/{{$news->id}}/{{$news->unsignedTitle}}.html"> {{$news->Title}} </a> </h3>
                            <p>{{$news->Summary}}</p>
                            <a class="btn btn-primary" href="news/{{$news->id}}/{{$news->unsignedTitle}}.html">Read more..<span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                        <div class="break"></div>
                    </div>
                @endforeach
                <div style="text-align:center">
                    {{$newses->links()}}
                </div>
            </div>
        </div>

    </div>

</div>
<!-- end Page Content -->

@endsection