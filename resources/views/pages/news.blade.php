@extends('layout.index')

@section('content')

<!-- Page Content -->
<div class="container">
    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-9">

            <!-- Blog Post -->

            <!-- Title -->
            <h1>{{$newses->Title}}</h1>

            <!-- Author -->
            <p class="lead">
                by Admin
            </p>

            <!-- Preview Image -->
            <img class="img-responsive" src="upload/news/{{$newses->Image}}" alt="">

            <!-- Date/Time -->
            <p><span class="glyphicon glyphicon-time"></span> Posted on {{$newses->created_at}}</p>
            <hr>

            <!-- Post Content -->
            <p class="lead">
                {!! $newses->Content !!}
            </p>

            <hr>

            <!-- Blog Comments -->

            <!-- Comments Form -->
            @if(Auth::user())
            <div class="well">
                <h4>Comment<span class="glyphicon glyphicon-pencil"></span></h4>
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <form action="comment/{{$newses->id}}" role="form" method="POST">
                    <div class="form-group">
                        <textarea class="form-control" name="Content" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            </div>

            <hr>
            @endif
            <!-- Posted Comments -->

            <!-- Comment -->
            @foreach($newses->comment as $comment)
            <div class="media">
                <a class="pull-left" href="news/{{$comment->id}}/{{$comment->unsignedTitle}}.html">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">{{$comment->user->name}}
                        <small>{{$comment->created_at}}</small>
                    </h4>
                    {{$comment->Content}}
                </div>
            </div>
            @endforeach

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-heading"><b>Release News</b></div>
                <div class="panel-body">
                    @foreach($releasenewses as $releasenews)
                        <!-- item -->
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                <a href="news/{{$releasenews->id}}/{{$releasenews->unsignedTitle}}.html">
                                    <img class="img-responsive" src="upload/news/{{$releasenews->Image}}" alt="">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <a href="news/{{$releasenews->id}}/{{$releasenews->unsignedTitle}}.html"><b>{{$releasenews->Title}}</b></a>
                            </div>
                            <p style="padding-left:5px">{{$releasenews->Summary}}</p>
                            <div class="break"></div>
                        </div>
                        <!-- end item -->
                    @endforeach
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><b>Hot News</b></div>
                <div class="panel-body">
                    @foreach($hotnewses as $hotnews)
                        <!-- item -->
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                <a href="news/{{$hotnews->id}}/{{$hotnews->unsignedTitle}}.html">
                                    <img class="img-responsive" src="upload/news/{{$hotnews->Image}}" alt="">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <a href="news/{{$hotnews->id}}/{{$hotnews->unsignedTitle}}.html"><b>{{$hotnews->Title}}</b></a>
                            </div>
                            <p style="padding-left:5px">{{$hotnews->Summary}}</p>
                            <div class="break"></div>
                        </div>
                        <!-- end item -->
                    @endforeach
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->
</div>
<!-- end Page Content -->

@endsection