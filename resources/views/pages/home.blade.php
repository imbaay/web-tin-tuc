@extends('layout.index')

@section('content')

<!-- Page Content -->
<div class="container">

    @include('layout.slide')

    <div class="space20"></div>


    <div class="row main-left">
        @include('layout.menu')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#337AB7; color:white;">
                    <h2 style="margin-top:0px; margin-bottom:0px;">Manh Tien</h2>
                </div>

                <div class="panel-body">
                    @foreach($categories as $category)
                        @if(count($category->type ) >0 )
                        <!-- item -->
                        <div class="row-item row">
                            <h3>
                                <a href="">{{$category->Name}}</a> 
                                @foreach($category->type as $type)
                                    <small>
                                        <a href="type/{{$type->id}}/{{$type->unsignedName}}.html"><i>{{$type->Name}}</i></a>
                                    </small>
                                @endforeach
                            </h3>
                            <?php
                                $data=$type->news->where('Highlight', 1)->sortByDesc('created_at')->take(5);
                                $news1=$data->shift();
                            ?>
                            <div class="col-md-8 border-right">
                                <div class="col-md-5">
                                    <a href="news/{{$news1['id']}}/{{$news1['unsignedTitle']}}.html">
                                        <img class="img-responsive" src="upload/news/{{$news1['Image']}}" alt="">
                                    </a>
                                </div>

                                <div class="col-md-7">
                                    <h3> <a href="news/{{$news1['id']}}/{{$news1['unsignedTitle']}}.html"> {{$news1['Title']}} </a> </h3>
                                    <p>{{$news1['Summary']}}</p>
                                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, quo, minima, inventore voluptatum saepe quos nostrum provident .</p> -->
                                    <a class="btn btn-primary" href="news/{{$news1['id']}}/{{$news1['unsignedTitle']}}.html">Read more.. <span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>

                            </div>


                            <div class="col-md-4">
                                @foreach($data->all() as $type)
                                <a href="news/{{$type['id']}}/{{$type['unsignedTitle']}}.html">
                                    <h4>
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                        {{$type['Title']}}
                                    </h4>
                                </a>
                                @endforeach
                            </div>

                            <div class="break"></div>
                        </div>
                        <!-- end item -->
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- end Page Content -->

@endsection