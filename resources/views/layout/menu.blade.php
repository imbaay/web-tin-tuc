<div class="col-md-3 ">
    <ul class="list-group" id="menu">
        <li href="#" class="list-group-item menu1 active">
            Menu
        </li>

        @foreach($categories as $category)
            @if(count($category->type)>0)
                <li href="#" class="list-group-item menu1">
                    {{$category->Name}}
                </li>
                <ul>
                    @foreach($category->type as $types)
                        <li class="list-group-item">
                            <a href="type/{{$types->id}}/{{$types->unsignedName}}.html">{{$types->Name}}</a>
                        </li>
                    @endforeach
                </ul>
            @endif
        @endforeach
    </ul>
</div>