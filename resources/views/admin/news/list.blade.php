@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">News
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Category</th>
                        <th>Category Type</th>
                        <th>View</th>
                        <th>Highlight</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                
                    @foreach($newses as $news)
                        <tr class="odd gradeX" align="center">
                            <td>{{$news->id}}</td>
                            <td>
                                <p>{{$news->Title}}</p>
                                <img style="width:120px" src="upload/news/{{$news->Image}}" />
                            </td>
                            
                            <td>{{$news->Summary}}</td>
                            <td>{{$news->type->category->Name}}</td>
                            <td>{{$news->type->Name}}</td>
                            <td>{{$news->View}}</td>
                            <td>
                                @if($news->Highlight == 0)
                                    {{'No'}}
                                @else
                                    {{'Yes'}}
                                @endif
                            </td>
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/news/delete/{{$news->id}}"> Delete</a></td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/news/edit/{{$news->id}}">Edit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection