@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">News
                    <small>Add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{$error}} <br>
                        @endforeach
                    </div>
                @endif
                @if(session('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
                <form action="admin/news/add" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" name="Category" id="Category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->Name}}</option>
                            @endforeach
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Category Type</label>
                        <select class="form-control" name="Type" id="Type">
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->Name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" name="Title" placeholder="Title" />
                    </div>
                    <div class="form-group">
                        <label>Summary</label>
                        <textarea name="Summary" id="demo" class="form-control ckeditor" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea name="Content" id="demo" class="form-control ckeditor" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input name="Image" type="file" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label>Highlight</label>
                        <label class="radio-inline">
                            <input name="Highlight" value="1" checked="" type="radio">Yes
                        </label>
                        <label class="radio-inline">
                            <input name="Highlight" value="0" type="radio">No
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('script')

    <script>
        $(document).ready(function(){
            $("#Category").change(function(){
                var idCategory=$(this).val();
                //alert (idCategory);
                $.get("admin/ajax/type/"+idCategory, function(data){
                    $("#Type").html(data);
                });
            });
        });
    </script>

@endsection