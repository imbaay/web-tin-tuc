@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User
                    <small>{{$users->name}}</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                    {{$error}} <br>
                    @endforeach
                </div>
                @endif
                @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
                @endif
                <form action="admin/user/edit/{{$users->id}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="name" value="{{$users->name}}" placeholder="Username" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" value="{{$users->email}}" readonly="" placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="changePassword" name="changePassword" />
                        <label>Change Password</label>
                        <input class="form-control password" type="password" name="password" placeholder="Enter Password" disabled="" />
                    </div>
                    <div class="form-group">
                        <label>Re-enter Password</label>
                        <input class="form-control password" type="password" name="passwordAgain" placeholder="Enter Password Again" disabled="" />
                    </div>

                    <div class="form-group">
                        <label>User Level</label>
                        <label class="radio-inline">
                            <input name="level" value="0" type="radio" @if($users->level==0)
                            {{'checked'}}
                            @endif
                            >Normal
                        </label>
                        <label class="radio-inline">
                            <input name="level" value="1" type="radio" @if($users->level==1)
                            {{'checked'}}
                            @endif
                            >Admin
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Edit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $("#changePassword").change(function() {
                if($(this).is(":checked"))
                {
                    $(".password").removeAttr("disabled");
                }
                else
                {
                    $(".password").attr("disabled","");
                }
            });
        });
    </script>

@endsection