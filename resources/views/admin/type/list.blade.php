@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Types
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            @endif
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Unsigned Name</th>
                        <th>Category</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($types as $type)
                    <tr class="odd gradeX type{{$type->id}}" align="center">
                        <td>{{$type->id}}</td>
                        <td>{{$type->Name}}</td>
                        <td>{{$type->unsignedName}}</td>
                        <td>{{$type->category->Name}}</td>
                        <td>
                            <button type="button" class="btn btn-danger delete-modal" data-id="{{$type->id}}" data-name="{{$type->Name}}" data-unsigned="{{$type->unsignedName}}" data-category="{{$type->category->Name}}">Delete
                            </button>
                        </td>
                        <td class="center"><a href="admin/type/edit/{{$type->id}}" class="btn btn-warning">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<!-- Modal form to delete a form -->
<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-name"></h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center">Are you sure you want to delete this division?</h3>
                <br />
                <form class="form-horizontal" role="form">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="id">ID:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="id_delete" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="name">Name</label>
                        <div class="col-sm-9">
                            <input type="name" class="form-control" id="name_delete" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="name">UnsignedName</label>
                        <div class="col-sm-9">
                            <input type="name" class="form-control" id="unsignedName_delete" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="name">Category</label>
                        <div class="col-sm-9">
                            <input type="name" class="form-control" id="category_delete" disabled>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-trash'></span> Delete
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function() {
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));
            $('#name_delete').val($(this).data('name'));
            $('#unsignedName_delete').val($(this).data('unsigned'));
            $('#category_delete').val($(this).data('category'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'GET',
                url: 'admin/type/delete/' + id,
                dataType: 'json',
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                done: function(data) {
                    $('.type' + data['id']).remove();
                }
            })
        })
    });
</script>
@endsection