<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'UserController@getLoginAdmin');
Route::post('admin/login', 'UserController@postLoginAdmin');
Route::get('admin/logout', 'UserController@getLogoutAdmin');

Route::group(['prefix' => 'admin', 'middleware' => 'adminLogin'], function () {
    Route::get('home', 'HomeAdminController@home');
    Route::group(['prefix' => 'category'], function () {
        Route::get('list', 'CategoryController@getList');

        Route::get('add', 'CategoryController@getAdd');
        Route::post('add', 'CategoryController@postAdd');

        Route::get('edit/{id}', 'CategoryController@getEdit');
        Route::post('edit/{id}', 'CategoryController@postEdit');

        Route::get('delete/{id}', 'CategoryController@getDelete');
    });

    Route::group(['prefix' => 'type'], function () {
        Route::get('list', 'CategoryTypeController@getList');

        Route::get('add', 'CategoryTypeController@getAdd');
        Route::post('add', 'CategoryTypeController@postAdd');

        Route::get('edit/{id}', 'CategoryTypeController@getEdit');
        Route::post('edit/{id}', 'CategoryTypeController@postEdit');

        Route::get('delete/{id}', 'CategoryTypeController@getDelete');
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('list', 'NewsController@getList');

        Route::get('add', 'NewsController@getAdd');
        Route::post('add', 'NewsController@postAdd');

        Route::get('edit/{id}', 'NewsController@getEdit');
        Route::post('edit/{id}', 'NewsController@postEdit');

        Route::get('delete/{id}', 'NewsController@getDelete');
    });

    Route::group(['prefix' => 'comment'], function () {
        Route::get('delete/{id}/{idType}', 'CommentController@getDelete');
    });

    Route::group(['prefix' => 'slide'], function () {
        Route::get('list', 'SlideController@getList');

        Route::get('add', 'SlideController@getAdd');
        Route::post('add', 'SlideController@postAdd');

        Route::get('edit/{id}', 'SlideController@getEdit');
        Route::post('edit/{id}', 'SlideController@postEdit');

        Route::get('delete/{id}', 'SlideController@getDelete');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('list', 'UserController@getList');

        Route::get('add', 'UserController@getAdd');
        Route::post('add', 'UserController@postAdd');

        Route::get('edit/{id}', 'UserController@getEdit');
        Route::post('edit/{id}', 'UserController@postEdit');

        Route::get('delete/{id}', 'UserController@getDelete');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::get('type/{idCategory}', 'AjaxController@getType');
    });
});

Route::get('home', 'PagesController@home');
Route::get('contact', 'PagesController@contact');
Route::get('type/{id}/{unsignedName}.html', 'PagesController@getPostsByCategory');
Route::get('news/{id}/{unsignedTitle}.html', 'PagesController@News');

Route::get('login', 'PagesController@getLogin');
Route::post('login', 'PagesController@postLogin');
Route::get('logout', 'PagesController@getLogout');

Route::post('comment/{id}', 'CommentController@postComment');