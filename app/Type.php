<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = "type";
    public function category()
    {
        return $this->belongsTo('App\Category', 'idCategory', 'id');
    }
    public function news()
    {
        return $this->hasMany('App\News', 'idType', 'id');
    }
}
