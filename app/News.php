<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";
    public function type()
    {
        return $this->belongsTo('App\Type', 'idType', 'id');
    }
    public function comment()
    {
        return $this->hasMany('App\Comment', 'idNews', 'id');
    }
}
