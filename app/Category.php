<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    public function type()
    {
        return $this->hasMany('App\Type', 'idCategory', 'id');
    }
    public function news()
    {
        return $this->hasManyThrough('App\News', 'App\Type', 'idCategory', 'idType', 'id');
    }

    public static function abc($id)
    {
    	return Category::find($id);
    }
    
}
