<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Type;
use App\News;
use App\Comment;
use App\Http\Requests\NewsRequests;

class NewsController extends Controller
{
    public function getList()
    {
        $category = Category::all();
        $type = Type::all();
        $news = News::orderBy('id')->get();
        return view('admin.news.list', ['newses' => $news, 'categories' => $category, 'types' => $type]);
    }

    public function getAdd()
    {
        $category = Category::all();
        $type = Type::all();
        return view('admin.news.add', ['categories' => $category, 'types' => $type]);
    }
    public function postAdd(NewsRequests $request)
    {
        $validated = $request->validated();
        $news = new News;
        $news->Title = $request->Title;
        $news->Summary = $request->Summary;
        $news->Content = $request->Content;
        $news->unsignedTitle = changeTitle($request->Title);
        $news->idType = $request->Type;
        $news->View = 0;

        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $endOfFile = $file->getClientOriginalExtension();
            if ($endOfFile != 'jpg' && $endOfFile != 'png' && $endOfFile != 'jpeg') {
                return redirect('admin/news/add');
            }
            $name = $file->getClientOriginalName();
            $Image = str_random(5) . "_" . $name;
            while (file_exists("upload/news/" . $Image)) {
                $Image = str_random(5) . "_" . $name;
            }
            $file->move('upload/news', $Image);
            $news->Image = $Image;
        } else {
            $news->Image = "";
        }
        $news->save();
        return redirect('admin/news/add')->with('message', 'Add successfully');
    }

    public function getEdit($id)
    {
        $category = Category::all();
        $type = Type::all();
        $news = News::find($id);
        return view('admin.news.edit', ['newses' => $news, 'categories' => $category, 'types' => $type]);
    }
    public function postEdit(NewsRequests $request, $id)
    {
        $news = News::find($id);
        $validated = $request->validated();
        $news->Title = $request->Title;
        $news->Summary = $request->Summary;
        $news->Content = $request->Content;
        $news->unsignedTitle = changeTitle($request->Title);
        $news->idType = $request->Type;

        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $endOfFile = $file->getClientOriginalExtension();
            if ($endOfFile != 'jpg' && $endOfFile != 'png' && $endOfFile != 'jpeg') {
                return redirect('admin/news/add');
            }
            $name = $file->getClientOriginalName();
            $Image = str_random(5) . "_" . $name;
            while (file_exists("upload/news/" . $Image)) {
                $Image = str_random(5) . "_" . $name;
            }
            $file->move('upload/news', $Image);
            unlink("upload/news/" . $news->Image);
            $news->Image = $Image;
        }
        $news->save();
        return redirect('admin/news/edit/' . $id)->with('message', 'Edit successfully');
    }

    public function getDelete($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect('admin/news/list')->with('message', 'Delete successfully');
    }
}
