<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function getDelete($id, $idNews)
    {
        $comment = Comment::find($id);
        $comment->delete();

        return redirect('admin/news/edit/' . $idNews)->with('message', 'Comment deleted successfully');
    }

    public function postComment($id, Request $request)
    {
        $idNews=$id;
        $comment=new Comment;
    	$news=News::find($id);
    	$comment=new Comment;
    	$comment->idNews=$idNews;
    	$comment->idUSer=Auth::user()->id;
    	$comment->Content=$request->Content;
    	$comment->save();
    	// return redirect("news/$id/".$news->unsignedTitle.".html")->with('message','Comment posted');
    }
}
