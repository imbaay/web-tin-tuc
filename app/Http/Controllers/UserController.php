<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequests;
use App\Http\Requests\LoginRequests;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function getList()
    {
        $user = User::all();
        return view('admin.user.list', ['users' => $user]);
    }

    public function getAdd()
    {
        return view('admin.user.add');
    }
    public function postAdd(UserRequests $request)
    {
        $validated = $request->validated();
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->level = $request->level;

        $user->save();

        return redirect('admin/user/add')->with('message', 'Add successfully');
    }

    public function getEdit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', ['users' => $user]);
    }
    public function postEdit(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'name' => 'required|min:3'
            ]
        );

        $user = User::find($id);
        $user->name = $request->name;
        $user->level = $request->level;
        if ($request->changePassword == "on") {
            $this->validate(
                $request,
                [
                    'password' => 'required|min:6|max:32',
                    'passwordAgain' => 'required|same:password'
                ],
                [
                    'password.required' => 'Password is required',
                    'password.min' => 'Password must have at least 6 characters',
                    'password.max' => 'Password must have at least 3 characters',
                    'passwordAgain.required' => 'You have not re-entered the password',
                    'passwordAgain.same' => 'The password entered is not correct'
                ]
            );

            $user->password = bcrypt($request->password);
        }

        $user->save();

        return redirect('admin/user/edit/' . $id)->with('message', 'Edit successfully');
    }

    public function getDelete($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('admin/user/list')->with('message', 'Delete successfully');
    }

    //login
    public function getLoginAdmin()
    {
        return view('admin.login');
    }
    public function postLoginAdmin(LoginRequests $request)
    {
        $validated = $request->validated();
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('admin/home')->with('message', 'Login successfully');
        } else {
            return redirect('admin/login')->with('message', 'Login fails');
        }
    }

    public function getLogoutAdmin()
    {
        Auth::logout();
        return redirect('admin/login')->with('message', 'Logout successfully');
    }
}
