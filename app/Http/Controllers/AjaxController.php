<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Type;

class AjaxController extends Controller
{
    public function getType($idCategory)
    {
        $type = Type::where('idCategory', $idCategory)->get();
        foreach ($type as $types) {
            echo "<option value='" . $types->id . "'>" . $types->Name . "</option>";
        }
    }
}
