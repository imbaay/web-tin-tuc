<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Http\Requests\CategoryRequests;

class CategoryController extends Controller
{
    public function getList()
    {
        $category = Category::all();
        return view('admin.category.list', ['categories' => $category]);
    }

    public function getAdd()
    {
        return view('admin.category.add');
    }
    public function postAdd(CategoryRequests $request)
    {
        $validated = $request->validated();
        $category = new Category;
        $category->Name = $request->Name;
        $category->unsignedName = changeTitle($request->Name);
        $category->save();
        return redirect('admin/category/add')->with('message', 'Add successfully');
    }

    public function getEdit($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit', ['categories' => $category]);
    }
    public function postEdit(CategoryRequests $request, $id)
    {
        $category = Category::abc($id);
        $validated = $request->validated();
        $category->Name = $request->Name;
        $category->unsignedName = changeTitle($request->Name);
        $category->save();
        return redirect('admin/category/edit/' . $id)->with('message', 'Edit successfully');
    }

    public function getDelete($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('admin/category/list')->with('message', 'Delete successfully');
    }
}
