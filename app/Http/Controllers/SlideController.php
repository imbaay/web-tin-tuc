<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;

class SlideController extends Controller
{
    public function getList()
    {
        $slide = Slide::all();
        return view('admin.slide.list', ['slides' => $slide]);
    }

    public function getAdd()
    {
        return view('admin.slide.add');
    }
    public function postAdd(Request $request)
    {
        $this->validate(
            $request,
            [
                'Name' => 'required',
                'Content' => 'required'
            ],
            [
                'Name.required' => 'Bạn chưa nhập tên',
                'Content.required' => 'Bạn chưa nhập nội dung'
            ]
        );

        $slide = new Slide();
        $slide->Name = $request->Name;
        $slide->Content = $request->Content;
        if ($request->has("link"))
            $slide->link = $request->link;

        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
                return redirect('admin/slide/add')->with('message', 'File phải có đuôi jpg,jpeg,png');
            }
            $name = $file->getClientOriginalName();
            $Image = str_random(5) . "_" . $name;
            while (file_exists("upload/slide/" . $Image)) {
                $Image = str_random(5) . "_" . $name;
            }
            $file->move('upload/slide', $Image);
            $slide->Image = $Image;
        } else {
            $slide->Image = "";
        }

        $slide->save();

        return redirect('admin/slide/add')->with('message', 'Add successfully');
    }

    public function getEdit($id)
    {
        $slide = Slide::find($id);
        return view('admin.slide.edit', ['slides' => $slide]);
    }
    public function postEdit(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'Name' => 'required',
                'Content' => 'required'
            ],
            [
                'Name.required' => 'Bạn chưa nhập tên',
                'Content.required' => 'Bạn chưa nhập nội dung'
            ]
        );

        $slide = Slide::find($id);
        $slide->Name = $request->Name;
        $slide->Content = $request->Content;
        if ($request->has("link"))
            $slide->link = $request->link;

        if ($request->hasFile('Image')) {
            $file = $request->file('Image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
                return redirect('admin/slide/add')->with('message', 'File phải có đuôi jpg,jpeg,png');
            }
            $name = $file->getClientOriginalName();
            $Image = str_random(5) . "_" . $name;
            while (file_exists("upload/slide/" . $Image)) {
                $Image = str_random(5) . "_" . $name;
            }
            unlink("upload/slide/" . $slide->Image);
            $file->move('upload/slide', $Image);
            $slide->Image = $Image;
        } else {
            $slide->Image = "";
        }

        $slide->save();

        return redirect('admin/slide/edit/' . $id)->with('message', 'Edit successfully');
    }

    public function getDelete($id)
    {
        $slide = Slide::find($id);
        $slide->delete();

        return redirect('admin/slide/list')->with('message', 'Delete successfully');
    }
}
