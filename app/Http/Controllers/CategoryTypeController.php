<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Type;
use App\Category;
use App\Http\Requests\CategoryTypeRequests;

class CategoryTypeController extends Controller
{
    public function getList()
    {
        $type = Type::all();
        return view('admin.type.list', ['types' => $type]);
    }

    public function getAdd()
    {
        $category = Category::all();
        return view('admin.type.add', ['categories' => $category]);
    }

    public function postAdd(CategoryTypeRequests $request)
    {
        $validated = $request->validated();
        $type = new Type;
        $type->Name = $request->Name;
        $type->unsignedName = changeTitle($request->Name);
        $type->idCategory = $request->Category;
        $type->save();
        return redirect('admin/type/add')->with('message', 'Add successfully');
    }

    public function getEdit($id)
    {
        $category = Category::all();
        $type = Type::find($id);
        return view('admin.type.edit', ['types' => $type], ['categories' => $category]);
    }

    public function postEdit(CategoryTypeRequests $request, $id)
    {
        $validated = $request->validated();
        $type = Type::find($id);
        $type->Name = $request->Name;
        $type->unsignedName = changeTitle($request->Name);
        $type->idCategory = $request->Category;
        $type->save();
        return redirect('admin/type/edit/' . $id)->with('message', 'Edit successfully');
    }

    public function getDelete($id)
    {
        $type = Type::find($id);
        $type->delete();
        return response()->json($type);
    }
}
